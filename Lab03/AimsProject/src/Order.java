public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 10){
			System.out.println("Your order is already full.");
			return;
		}
		itemsOrdered[qtyOrdered] = disc;
		qtyOrdered++;
		System.out.println("Order has been updated. Your order now has "+ qtyOrdered + " discs");
	}

	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		if (qtyOrdered == 0){
			System.out.println("Your order has no discs to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < qtyOrdered; i++) {
			if (itemsOrdered[i] == disc){
				search = i;
				break;
			}
		}
		if (search != -1){
			itemsOrdered[search] = null;
			for (int i = search; i < qtyOrdered-1; i++) {
				itemsOrdered[i] = itemsOrdered[i + 1];
			}
			qtyOrdered--;
		}
		else
			System.out.println("Disc not found!");
		
		System.out.println("Order has been updated. Your order now has "+ qtyOrdered + " discs");
	}

	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < qtyOrdered; i++) {
			sum += itemsOrdered[i].getCost();
		}
		return sum;
	}
}