import java.time.LocalDate;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class MyDate {
    int day = 0;
    int month = 0;
    int year = 0;

    public MyDate() {
        LocalDate currentdate = LocalDate.now();
        this.day = currentdate.getDayOfMonth();
        this.month = currentdate.getMonthValue();
        this.year = currentdate.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String inputDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy");
        LocalDate dateString = LocalDate.parse(inputDate, formatter);

        // Get day from date
        this.day = dateString.getDayOfMonth();

        // Get month from date
        this.month = dateString.getMonthValue();

        // Get year from date
        this.year = dateString.getYear();
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;

    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void accept() {
        System.out.println("Please enter the date in the format 'MMMM d yyyy': ");
        Scanner s = new Scanner(System.in);
        String inputDate = s.nextLine();
        MyDate date = new MyDate(inputDate);
        this.day = date.day;
        this.month = date.month;
        this.year = date.year;
    }

    public void print() {
        System.out.println("Today is: " + this.getDay() + "/" + this.getMonth() + "/" + this.getYear());
    }

    // Driver Code
    public static void main(String[] args) {
        MyDate md = new MyDate();
        md.print();
        md.accept();
        md.print();
    }

}
