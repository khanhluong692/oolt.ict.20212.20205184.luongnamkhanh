package hust.soict.globalict.aims.order;
import java.util.ArrayList;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;


public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public int idOrder;
	private MyDate dateOrdered;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public int luckyNumber=-1;
	private static int nbOrders = 0;
	public static final int MAX_LIMITED_ORDERS = 5;
	
	public void addMedia(Media media) {
		if (itemsOrdered.size() == MAX_NUMBERS_ORDERED){
			System.out.println("Your order is already full.");
			return;
		}
//		media.setId(itemsOrdered.size() + 1);
		itemsOrdered.add(media);
		System.out.println("Order has been updated. Your order now has "+ itemsOrdered.size() + " discs");
	}
	public void removeMedia(int id){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getId() == id)
                search = i;
        }
        if (search == -1)
            System.out.println("Disc not found!");
        else{
            itemsOrdered.remove(search);
			
            System.out.println("Media removed successfully!");
        }
	}

	public void removeMedia(Media media){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no discs to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).getTitle().equals(media.getTitle())){
				search = i;
				break;
			}
		}
		if (search != -1){
			itemsOrdered.remove(search);
//			for (int i = search; i < itemsOrdered.size(); i++) {
//				itemsOrdered.get(i).setId(i+1);
//			}
		}
		else
			System.out.println("Disc not found!");
		
		System.out.println("Order has been updated. Your order now has "+ itemsOrdered.size() + " discs");
	}
	public float totalCost() {
		
		float sum = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (i!= luckyNumber)
				sum += itemsOrdered.get(i).getCost();
			else
				continue;
		}
		return sum;
	}
//	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
//		int i = 0;
//		while (qtyOrdered <= 10 && i < dvdList.length) {
//			addDigitalVideoDisc(dvdList[i]);
//			i++;
//		}
//		if (qtyOrdered > 10) {
//			System.out.println("Your order is already full.");
//			for (int j = i; j < dvdList.length; j++) {
//				System.out.println(dvdList[j].getCategory() + "-" + dvdList[j].getCost() + "-"
//						+ dvdList[j].getDirector() + "-" + dvdList[j].getLength());
//			}
//		}
//
//	}
//
//	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
//		if (qtyOrdered == 10) {
//			System.out.println("Your order is already full.");
//			return;
//		}
//		DigitalVideoDisc[] dvdList = { dvd1, dvd2 };
//		addDigitalVideoDisc(dvdList);
//	}

	public Order() {

		if (getNbOrders() < MAX_LIMITED_ORDERS) {
			setNbOrders(getNbOrders() + 1);
			MyDate date = new MyDate();
			int id = getNbOrders();
			this.dateOrdered = date;
			this.idOrder= id;
			System.out.println("There's a new order has been created!" );

		} else
			System.out.println("You can not add any order due to limitation reached");

	}
//	public void orderPrint() {
//		System.out.println("***********************Order"+nbOrders+"***********************");
//		System.out.print("Date: ");
//		dateOrdered.print();
//		System.out.println("Ordered Items:");
//		for (int i = 0; i < qtyOrdered; i++) {
//			if (i==luckyNumber)
//				System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": 0$");
//			else
//				System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": "+ itemsOrdered[i].getCost() + "$");
//		}
//		System.out.println("Total cost: " + this.totalCost());
//		System.out.println("***************************************************");
//	}
	public void orderPrint() {
		System.out.println("***********************Order"+this.idOrder+"***********************");
		System.out.print("Date: ");
		dateOrdered.print();
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (i==luckyNumber)
				System.out.println(itemsOrdered.get(i).getId() + "." + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory()+ ": 0$");
			else
				System.out.println(itemsOrdered.get(i).getId()+ "." + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory()+ ": "+ itemsOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}
	public Media getALuckyItem() {
		luckyNumber= (int)(Math.random()*itemsOrdered.size());
		return itemsOrdered.get(luckyNumber);
		}

	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	
}