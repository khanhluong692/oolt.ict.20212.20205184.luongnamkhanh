package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	// Order anOrder = new Order();
	// DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	// dvd1.setCategory("Animation");
	// dvd1.setCost(19.95f);
	// dvd1.setDirector("Roger Allers");
	// dvd1.setLength(87);
	// //add the dvd to the order
	// anOrder.addDigitalVideoDisc(dvd1);
	//
	// DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
	// dvd2.setCategory("Science Fiction");
	// dvd2.setCost(24.95f);
	// dvd2.setDirector("George Lucas");
	// dvd2.setLength(124);
	// anOrder.addDigitalVideoDisc(dvd2);
	//
	// DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
	// dvd3.setCategory("Animation");
	// dvd3.setCost(18.99f);
	// dvd3.setDirector("John Musker");
	// dvd3.setLength(90);
	//
	// anOrder.addDigitalVideoDisc(dvd3);
	//
	// System.out.print("Total cost is: ");
	// System.out.println(anOrder.totalCost());
	// //Test removeDigitalVideoDisc
	// anOrder.removeDigitalVideoDisc(dvd3);
	// System.out.println("The total cost after remove dvd3 is: "+
	// anOrder.totalCost());
	//
	//
	// DigitalVideoDisc[] listDVD1 = new DigitalVideoDisc[3];
	// listDVD1[0]= new DigitalVideoDisc("abc1", "abc","abc", 0, 0);
	// listDVD1[1]= new DigitalVideoDisc("abc2", "abc","abc", 0, 0);
	// listDVD1[2]= new DigitalVideoDisc("abc3", "abc","abc", 0, 0);
	// anOrder.addDigitalVideoDisc(listDVD1);
	// anOrder.addDigitalVideoDisc(dvd3,dvd2);
	//
	//
	// anOrder.orderPrint();
	// //
	// System.out.println("Order 2: ");
	// Order anOrder2 = new Order();
	// DigitalVideoDisc dvd5= new DigitalVideoDisc("Harry
	// Potter","Fiction","J.K.Rowlling",120,30);
	// anOrder2.addDigitalVideoDisc(dvd5);
	// anOrder2.orderPrint();
	// //
	// System.out.println("Order 3: ");
	// Order anOrder3 = new Order();
	// DigitalVideoDisc dvd6 = new DigitalVideoDisc("Jurassic Park", "Science
	// Fiction", "Steven Spielberg", 30, 20);
	// anOrder3.addDigitalVideoDisc(dvd6);
	// anOrder3.orderPrint();
	public static void showMenu() {

		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}

	public static void main(String[] args) {

		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		int IdOrderSearch = -1;
		do {
			showMenu();
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				Order newOrder = new Order();
				userOrder.add(newOrder);
				break;
			case 2:
				System.out.println("Input the id of the order you want to add items to: ");
				sc = new Scanner(System.in);
				int inputIdOrders = sc.nextInt();
				IdOrderSearch = -1;
				for (int i = 0; i < userOrder.size(); i++) {
					if (userOrder.get(i).idOrder == inputIdOrders)
						IdOrderSearch = i;
				}

				if (IdOrderSearch == -1) {
					System.out.println("Wrong idOrder!");
					break;
				}

				System.out.println("Input the number of the items you want to add: ");
				sc = new Scanner(System.in);
				int inputNum = sc.nextInt();
				for (int i = 0; i < inputNum; i++) {
					Media newMedia = new Media();
					System.out.println("Enter Id: ");
					sc = new Scanner(System.in);
					int id = sc.nextInt();
					System.out.println("Enter title: ");
					sc = new Scanner(System.in);
					String title = sc.nextLine();
					System.out.println("Enter category: ");
					sc = new Scanner(System.in);
					String category = sc.nextLine();
					System.out.println("Enter cost: ");
					sc = new Scanner(System.in);
					float cost = sc.nextFloat();
					newMedia.setId(id);
					newMedia.setTitle(title);
					newMedia.setCategory(category);
					newMedia.setCost(cost);
					userOrder.get(IdOrderSearch).addMedia(newMedia);
				}
				break;

			case 3:
				System.out.println("Input the id of the order you want to delete item from: ");
				sc = new Scanner(System.in);
				inputIdOrders = sc.nextInt();
				IdOrderSearch = -1;
				for (int i = 0; i < userOrder.size(); i++) {
					if (userOrder.get(i).idOrder == inputIdOrders)
						IdOrderSearch = i;
				}
				if (IdOrderSearch == -1) {
					System.out.println("Wrong id order!");
					break;
				}

				System.out.println("Input the id of the item you want to delete: ");
				sc = new Scanner(System.in);
				int itemId = sc.nextInt();
				userOrder.get(IdOrderSearch).removeMedia(itemId);
				break;

			case 4:
				System.out.println("Input the id order of the order you want to display: ");
				sc = new Scanner(System.in);
				inputIdOrders = sc.nextInt();
				IdOrderSearch = -1;
				for (int i = 0; i < userOrder.size(); i++) {
					if (userOrder.get(i).idOrder == inputIdOrders)
						IdOrderSearch = i;
				}
				if (IdOrderSearch == -1) {
					System.out.println("Wrong id number!");
					break;
				}
				userOrder.get(IdOrderSearch).orderPrint();
				break;

			case 0:
				System.exit(0);
				return;

			}
		} while (choice != 0);
	}

}
