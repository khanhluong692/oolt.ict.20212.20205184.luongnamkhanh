package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder= new Order(); 
		DigitalVideoDisc dvd1= new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		anOrder.addMedia(dvd1);
		
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.01f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124); 
		anOrder.addMedia(dvd2);
		
		DigitalVideoDisc dvd3= new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(19.01f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90); 
		anOrder.addMedia(dvd3);
		
		//Check the search function
		System.out.println(dvd1.search("The Lion"));
		System.out.println(dvd1.search("King Lion"));
		System.out.println(dvd1.search("King          The"));
		System.out.println(dvd1.search("The King"));
		System.out.println("\n");
		
		//Get the lucky and free item
		
		System.out.println("Congratulations!! You can have Book :["+anOrder.getALuckyItem().getTitle()+"] for free \n");
		
		//print out the order
		anOrder.orderPrint();
		
		//print the total cost
		anOrder.totalCost();
	}

}
