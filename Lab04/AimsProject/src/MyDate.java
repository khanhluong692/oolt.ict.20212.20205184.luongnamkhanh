import java.time.LocalDate;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.Month;

public class MyDate {
    int day = 0;
    int month = 0;
    int year = 0;

    public MyDate() {
        LocalDate currentdate = LocalDate.now();
        this.day = currentdate.getDayOfMonth();
        this.month = currentdate.getMonthValue();
        this.year = currentdate.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String inputDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d['st']['nd']['rd']['th'] yyyy");
        LocalDate dateString = LocalDate.parse(inputDate, formatter);

        // Get day from date
        this.day = dateString.getDayOfMonth();

        // Get month from date
        this.month = dateString.getMonthValue();

        // Get year from date
        this.year = dateString.getYear();
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;

    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    public LocalDate getLocalDate(){
        return LocalDate.of(year, month, day);
    }

    public void accept() {
        System.out.println("Please enter the date in the format 'MMMM d yyyy': ");
        Scanner s = new Scanner(System.in);
        String inputDate = s.nextLine();
        s.close();
        MyDate date = new MyDate(inputDate);
        this.day = date.day;
        this.month = date.month;
        this.year = date.year;
    }

    public void print() {
        System.out.println("Today is: " + this.getDay() + "/" + this.getMonth() + "/" + this.getYear());
    }
    public MyDate(String day, String month, String year) {
		String[] dayStrings = {"first", "second", "third", "fourth", "fifth", "sixth",
							   "seventh", "eighth", "ninth", "tenth", "eleventh",
							   "twelfth", "thirteenth", "fourteenth", "fifteenth",
							   "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth"
							};
		if(day.equals("thirtieth")) this.day = 30;
		if(day.equals("thirty-first"))this.day = 31;
		for(int i = 0; i < dayStrings.length; i++) {
			if(day.equals(dayStrings[i])) {
				this.day = i + 1;
				break;
			}
			if(day.equals("twenty " + dayStrings[i]) || day.equals("twenty-" + dayStrings[i])) {
				this.day = i + 21;
				break;
			}
		}
		this.month = Month.valueOf(month.toUpperCase()).getValue();
		String[] yearStrings = {"one", "two", "three", "four", "five",
								"six", "seven", "eight", "nine", "ten",
								"eleven", "twelve", "thirteen", "fourteen", "fifteen",
								"sixteen", "seventeen", "eighteen", "nineteen", "twenty"
							   };
		String[] yearStrings2 = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		String[] strs = year.split(" ");
		if(strs.length == 2 || strs.length == 3) {
			for(int i = 0; i < yearStrings.length; i++) {
				if(strs[0].equals(yearStrings[i])) {
					this.year = (i + 1)*100;
					break;
				}
			}
			if(strs[1].equals("thousand")) {
				this.year *= 10;
			}
			else if(!strs[1].equals("hundred")) {
				for(int i = 0; i < yearStrings.length; i++) {
					if(strs[strs.length-1].equals(yearStrings[i])) {
						this.year += i + 1;
						break;
					}
				}
				outer:
				for(int i = 0; i < yearStrings2.length; i++) {
					if(strs[1].equals(yearStrings2[i])) {
						this.year += (i + 2)*10;
						break outer;
					}else {
						for(int j = 0; j < 9; j++) {
							if(strs[1].equals(yearStrings2[i] + "-" + yearStrings[j])) {
								this.year += (i + 2)*10 + j + 1;
								break outer;
							}
						}
					}
				}
			}
		}
		if(strs.length == 4) {
			for(int i = 0; i < 10; i++) {
				if(strs[3].equals(yearStrings[i])) {
					this.year = 2001 + i;
				}
			}
		}
	}
    public void print(String format) {
        LocalDate date = LocalDate.of(this.year, this.month, this.day);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern(format);
        String formattedDate = date.format(myFormatObj);
        System.out.println(formattedDate);
    }
    // Driver Code
    public static void main(String[] args) {
        MyDate md = new MyDate();
        md.print();
        md.accept();
        md.print();
    }

}
