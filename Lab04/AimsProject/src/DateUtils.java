import java.time.LocalDate;
import java.util.Arrays;

public class DateUtils {
    public static void compareDate (MyDate date1, MyDate date2) {
        LocalDate datetime1 = date1.getLocalDate();
        LocalDate datetime2 = date2.getLocalDate();
        if (datetime1.compareTo(datetime2) > 0) {
            System.out.println("Date1 is after Date2");
        } else if (datetime1.compareTo(datetime2) < 0) {
            System.out.println("Date1 is before Date2");
        } else {
            System.out.println("Date1 is equal to Date2");
        }
    }
    public static int compareDateCondition (MyDate date1, MyDate date2){
        LocalDate datetime1 = date1.getLocalDate();
        LocalDate datetime2 = date2.getLocalDate();
        return datetime1.compareTo(datetime2);
    }

    public static void sortDate(MyDate dateArr[]){
        System.out.println("The array before being sorted is:");
        for (int i = 0; i < dateArr.length; i++) {
            dateArr[i].print();
        }
        
        Arrays.sort(dateArr, DateUtils::compareDateCondition);

        System.out.println("The array after being sorted is:");
        for (int i = 0; i < dateArr.length; i++) {
            dateArr[i].print();
        }
    }
}

