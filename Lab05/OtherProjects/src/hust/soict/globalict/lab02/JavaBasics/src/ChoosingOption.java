package hust.soict.globalict.lab02.JavaBasics.src;
import javax.swing.JOptionPane;
public class ChoosingOption {
    public static void main(String[] args){
        int Option = JOptionPane.showConfirmDialog(null, "Do u want to change to the first class ticket?");
        JOptionPane.showMessageDialog(null, "Youre chosen: " + (Option == JOptionPane.YES_OPTION?"Yes":"No"));
        System.exit(0);
    }
}
