package hust.soict.globalict.lab02.SortArray.src;
import java.util.Arrays;
import java.util.Scanner;
public class SortArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arrSize = 0, sum = 0;
        System.out.print("Enter number of elements: ");
        arrSize = sc.nextInt();
        sc.close();
        int[] arr = new int[arrSize];
        for(int i=0;i<arrSize;++i){
            System.out.print("Enter element #" + i + ": ");
            arr[i] = sc.nextInt();
            sum += arr[i];
        }
        Arrays.sort(arr);
        System.out.println("Sorted array: " + Arrays.toString(arr));
        System.out.println("Sum: " + sum);
        System.out.print("Average value: " + (double)sum / arrSize);

    }
}
