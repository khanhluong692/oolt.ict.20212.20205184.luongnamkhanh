package hust.soict.globalict.garbage;
import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors


public class TaoFile {
	public static void main(String[] args) throws Exception {
        try {
            FileWriter myWriter = new FileWriter("garbage.txt");
            int count = 0;
            for (int i = 0; i < 100000; i++) {
                myWriter.write("khanh" + count +"\n");
                count++;
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println("Success...");
    }
}
