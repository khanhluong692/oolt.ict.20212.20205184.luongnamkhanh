package hust.soict.globalict.test.utils;
import java.util.*;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        MyDate today = new MyDate();
        today.print();

        MyDate dateInt = new MyDate(18, 9, 2002);
        dateInt.print();

        MyDate dateStr = new MyDate("September 17th 2002");
        dateStr.print();

        MyDate input = new MyDate();
        input.accept();
        input.print();

        MyDate customFormat = new MyDate("second","September", "twenty nineteen");
        Scanner sc = new Scanner(System.in);
        System.out.println("Input the format of the showing date: ");
        String pattern = sc.nextLine();
        sc.close();
        customFormat.print(pattern);
        DateUtils.compareDate(today, dateInt);
        MyDate arrDate[] = {today, dateInt, dateStr};
        DateUtils.sortDate(arrDate);
    }
}
