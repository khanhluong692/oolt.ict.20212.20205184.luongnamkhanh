package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		//add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		anOrder.addDigitalVideoDisc(dvd3);
		
		System.out.print("Total cost is: ");
		System.out.println(anOrder.totalCost());
		//Test removeDigitalVideoDisc
		anOrder.removeDigitalVideoDisc(dvd3);
		System.out.println("The total cost after remove dvd3 is: "+ anOrder.totalCost());


		DigitalVideoDisc[] listDVD1 = new DigitalVideoDisc[3];
		listDVD1[0]= new DigitalVideoDisc("abc1", "abc","abc", 0, 0);
		listDVD1[1]= new DigitalVideoDisc("abc2", "abc","abc", 0, 0);
		listDVD1[2]= new DigitalVideoDisc("abc3", "abc","abc", 0, 0);
		anOrder.addDigitalVideoDisc(listDVD1);
		anOrder.addDigitalVideoDisc(dvd3,dvd2);
		

		anOrder.orderPrint();
		//
		System.out.println("Order 2: ");
		Order anOrder2 = new Order();
		DigitalVideoDisc dvd5= new DigitalVideoDisc("Harry Potter","Fiction","J.K.Rowlling",120,30);
		anOrder2.addDigitalVideoDisc(dvd5);
		anOrder2.orderPrint();
		//
		System.out.println("Order 3: ");
		Order anOrder3 = new Order();
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Jurassic Park", "Science Fiction", "Steven Spielberg", 30, 20);
		anOrder3.addDigitalVideoDisc(dvd6);
		anOrder3.orderPrint();
	}
}