package hust.soict.globalict.aims.order;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;

import hust.soict.globalict.aims.utils.MyDate;


public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	private MyDate dateOrdered;

	public int luckyNumber;
	private static int nbOrders = 0;
	public static final int MAX_LIMITED_ORDERS = 5;
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered == 10){
			System.out.println("Your order is already full.");
			return;
		}
		itemsOrdered[qtyOrdered] = disc;
		qtyOrdered++;
		System.out.println("Order has been updated. Your order now has "+ qtyOrdered + " discs");
	}

	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		if (qtyOrdered == 0){
			System.out.println("Your order has no discs to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < qtyOrdered; i++) {
			if (itemsOrdered[i] == disc){
				search = i;
				break;
			}
		}
		if (search != -1){
			itemsOrdered[search] = null;
			for (int i = search; i < qtyOrdered-1; i++) {
				itemsOrdered[i] = itemsOrdered[i + 1];
			}
			qtyOrdered--;
		}
		else
			System.out.println("Disc not found!");
		
		System.out.println("Order has been updated. Your order now has "+ qtyOrdered + " discs");
	}

	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < qtyOrdered; i++) {
			if (i!= luckyNumber)
				sum += itemsOrdered[i].getCost();
			else
				continue;
		}
		return sum;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		int i = 0;
		while (qtyOrdered <= 10 && i < dvdList.length) {
			addDigitalVideoDisc(dvdList[i]);
			i++;
		}
		if (qtyOrdered > 10) {
			System.out.println("Your order is already full.");
			for (int j = i; j < dvdList.length; j++) {
				System.out.println(dvdList[j].getCategory() + "-" + dvdList[j].getCost() + "-"
						+ dvdList[j].getDirector() + "-" + dvdList[j].getLength());
			}
		}

	}

	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if (qtyOrdered == 10) {
			System.out.println("Your order is already full.");
			return;
		}
		DigitalVideoDisc[] dvdList = { dvd1, dvd2 };
		addDigitalVideoDisc(dvdList);
	}
	public Order() {

		if (nbOrders < MAX_LIMITED_ORDERS) {
			nbOrders++;
			MyDate date = new MyDate();
			this.dateOrdered = date;
		} else
			System.out.println("You can not add any order due to limitation reached");

	}
	public void orderPrint() {
		System.out.println("***********************Order"+nbOrders+"***********************");
		System.out.print("Date: ");
		dateOrdered.print();
		System.out.println("Ordered Items:");
		for (int i = 0; i < qtyOrdered; i++) {
			if (i==luckyNumber)
				System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": 0$");
			else
				System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": "+ itemsOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}
	public DigitalVideoDisc getALuckyItem() {
		luckyNumber= (int)(Math.random()*qtyOrdered);
		return itemsOrdered[luckyNumber];
		}
	
}