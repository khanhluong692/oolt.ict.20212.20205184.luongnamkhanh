import java.util.Scanner;
public class Date {
    String[] monthList = {"Jan","Feb","Mar","Apr",
            "May","Jun","Jul","Aug","Sep","Oct",
            "Nov","Dec"};

    int countDays(int month, int year) {
        int count = -1;
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> count = 31;
            case 4, 6, 9, 11 -> count = 30;
            case 2 -> {
                if (year % 4 == 0) {
                    count = 29;
                } else {
                    count = 28;
                }
                if ((year % 100 == 0) & (year % 400 != 0)) {
                    count = 28;
                }
            }
        }
        return count;
    }

    boolean isInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        }
        catch(Exception exc){
            return false;
        }
    }

    public static void main(String[] args) {
        Date d = new Date();
        int year = 0, month = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter month: ");
        String Month = sc.nextLine();
        if(d.isInteger(Month)){
            month = Integer.parseInt(Month);
        }
        else{
            String cmp = Month.substring(0,3);
            for(int i=0;i<d.monthList.length;++i){
                if(cmp.equals(d.monthList[i])){
                    month = i + 1;
                    break;
                }
            }
        }

        System.out.print("Enter year: ");
        year = sc.nextInt();

        System.out.print(d.monthList[month-1] + ' ' + year + " has " + d.countDays(month,year) + " days.");
    }
}
