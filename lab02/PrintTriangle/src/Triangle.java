import java.util.Scanner;

public class Triangle {

    void printSpace(int n){
        for(int i=0;i<n;++i){
            System.out.print(' ');
        }
    }

    void printStar(int n){
        for(int i=0;i<n;++i){
            System.out.print('*');
        }
    }

    void printTriangle(int n){
        for(int i=1;i<=n;++i){
            this.printSpace(n - i);
            this.printStar(2 * i - 1);
            this.printSpace(n - i);
            System.out.print('\n');
        }
    }
    public static void main(String[] args){
        Triangle tri = new Triangle();
        int n = 0;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input n: ");
        n = keyboard.nextInt();
        tri.printTriangle(n);
    }
}
